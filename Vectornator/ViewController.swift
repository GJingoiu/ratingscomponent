//
//  ViewController.swift
//  Vectornator
//
//  Created by George Jingoiu on 19/01/2022.
//

import UIKit

class ViewController: UIViewController {
    
    private var valueLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let currentRating = CGFloat(3.0)
        
        let ratingView = RatingView(maximumRating: 5, currentRating: currentRating)
        ratingView.delegate = self
        self.view.addSubview(ratingView)
        
        NSLayoutConstraint.activate([
            ratingView.heightAnchor.constraint(equalToConstant: 50),
            ratingView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.5),
            ratingView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            ratingView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor)
        ])
        
        valueLabel = UILabel()
        valueLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(valueLabel)
        
        NSLayoutConstraint.activate([
            valueLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            valueLabel.topAnchor.constraint(equalTo: ratingView.bottomAnchor, constant: 50)
        ])
    }
}

extension ViewController: RatingViewDelegate {
    func ratingDidChange(_ ratingView: RatingView) {
        valueLabel.text = "\(ratingView.rating)"
    }
}

