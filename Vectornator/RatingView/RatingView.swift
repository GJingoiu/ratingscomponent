//
//  RatingView.swift
//  Vectornator
//
//  Created by George Jingoiu on 19/01/2022.
//

import UIKit

protocol RatingViewDelegate {
    //passing the whole object as there might also be info to identify WHO's rating changed. No need to pass the actual rating value as that can be read as the RatingView's property
    func ratingDidChange(_ ratingView: RatingView)
}

class RatingView: UIView {
    private(set) var maxRating: Int
    var rating: CGFloat {
        didSet {
            delegate?.ratingDidChange(self)
        }
    }
    var delegate: RatingViewDelegate?
    
    private var minimumPercentageIncrement: CGFloat //The rating value can go up and down in increments of X.
    private var coloredView: UIView
    private var coloredViewWidthAnchor: NSLayoutConstraint
    private var stackView: UIStackView
    
    /**
        `maximumRating`: The maxValue that the rating can have ( e.g. 5)
        `currentRating`: The starting value of the rating.
        `increments`: The rating can go up or down in increments. Most common is 0.5 ( half of star). Must be in 0...1
     */
    
    init(maximumRating: Int, currentRating: CGFloat = 0.0, increments: CGFloat = 0.01) {
        maxRating = maximumRating
        rating = currentRating
        
        /**Transform increments above expressed in star percentages into total width percentages.*/
        
        let controlIncrements = min(max(increments, 0.01), 1)
        //how many increments in 1 star. For 0.5 there's 2 increments in 1 star.
        let incrementsInStar = 1.0 / controlIncrements
        //how many increments needed for 100% rating?
        let numberOfIncrements = CGFloat(maximumRating) * incrementsInStar
        //What percentage of width would that make it?
        minimumPercentageIncrement = 100 / numberOfIncrements
        
        coloredView = UIView()
        coloredViewWidthAnchor = coloredView.widthAnchor.constraint(equalToConstant: 0)
        
        var starViews = [UIImageView]()
        for _ in 1...maxRating {
            let starBackground = UIImageView(image: UIImage(named: "starBackground"))
            starBackground.translatesAutoresizingMaskIntoConstraints = false
            starBackground.backgroundColor = .clear
            starBackground.widthAnchor.constraint(equalTo: starBackground.heightAnchor).isActive = true
            starViews.append(starBackground)
            
            let star = UIImageView(image: UIImage(named: "star"))
            star.translatesAutoresizingMaskIntoConstraints = false
            star.backgroundColor = .clear
            starBackground.addSubview(star)
            
            NSLayoutConstraint.activate([
                star.widthAnchor.constraint(equalTo: starBackground.widthAnchor),
                star.heightAnchor.constraint(equalTo: starBackground.heightAnchor),
                star.centerYAnchor.constraint(equalTo: starBackground.centerYAnchor),
                star.centerXAnchor.constraint(equalTo: starBackground.centerXAnchor),
            ])
            
        }
        
        stackView = UIStackView(arrangedSubviews: starViews)
        
        super.init(frame: CGRect.zero)
        translatesAutoresizingMaskIntoConstraints = false
        
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .center
        stackView.spacing = 0
        self.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            stackView.topAnchor.constraint(equalTo: self.topAnchor),
            stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        ])
        
        coloredView.translatesAutoresizingMaskIntoConstraints = false
        coloredView.backgroundColor = UIColor.systemBlue
        self.insertSubview(coloredView, belowSubview: stackView)
        
        NSLayoutConstraint.activate([
            coloredView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            coloredViewWidthAnchor,
            coloredView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            coloredView.heightAnchor.constraint(equalTo: starViews.first!.heightAnchor),
        ])
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateUIForRating(rating)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            updateRatingForTouchPosition(touch.location(in: self), roundUp: true)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            updateRatingForTouchPosition(touch.location(in: self))
        }
    }
    
    private func updateRatingForTouchPosition(_ position: CGPoint, roundUp: Bool = false) {
        var percentage = position.x / self.bounds.width
        percentage = percentage * 100
        
        var normalized = CGFloat(Int((percentage)/minimumPercentageIncrement)) * minimumPercentageIncrement
        if roundUp {
            //jump to the next increment. Needed when using the control more as a button than a slider.
            normalized = CGFloat(Int((percentage)/minimumPercentageIncrement) + 1) * minimumPercentageIncrement
        }
        normalized = max(normalized, 0)
        normalized = min(normalized, 100)
                
        rating = CGFloat(maxRating) * (normalized / 100.0)
        coloredViewWidthAnchor.constant = normalized/100.0 * self.bounds.width
    }
    
    private func updateUIForRating(_ newRating: CGFloat) {
        var percentage = 100.0 * newRating/CGFloat(maxRating)
        percentage = max(percentage, 0)
        percentage = min(percentage, 100)
        
        coloredViewWidthAnchor.constant = percentage/100.0 * self.bounds.width
    }
}
